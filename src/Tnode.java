import java.util.ArrayList;
import java.util.Stack;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public Tnode(String name) {
      String illegalChars = " ,()";
      if (name == null
              || name.isEmpty()
              || name.contains(illegalChars)
      ) throw new RuntimeException("\nInvalid node name: " + name);
      this.name = name;
   }

   @Override
   public String toString() {
      StringBuilder b = new StringBuilder();

      b.append(this.name);

      if (this.firstChild != null) b.append("(").append(firstChild.toString());
      if (this.nextSibling != null) b.append(",").append(nextSibling.toString()).append(")");

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<>();
      ArrayList<String> operators = new ArrayList<>(){{
         add("+");
         add("-");
         add("*");
         add("/");
         add("SWAP");
         add("ROT");
      }};
      String[] parts = pol.trim().split("\\s+");

      for (String s : parts) {
         Tnode node = new Tnode(s);
         if (operators.contains(s)) {
            if (stack.size() < 2) throw new RuntimeException("\nInvalid expression: " + pol);
            if (s.equals("SWAP")) {
               Tnode a = stack.pop();
               Tnode b = stack.pop();
               stack.push(a);
               stack.push(b);
               continue;
            } else if (s.equals("ROT")) {
               if (stack.size() < 3) throw new RuntimeException("\nInvalid expression: " + pol);
               Tnode a = stack.pop();
               Tnode b = stack.pop();
               Tnode c = stack.pop();
               stack.push(b);
               stack.push(a);
               stack.push(c);
               continue;
            } else {
               node.nextSibling = stack.pop();
               node.firstChild = stack.pop();
            }
         } else if (!isNumeric(s)) {
            throw new RuntimeException("\nInvalid expression: " + pol);
         }
         stack.push(node);
      }

      if (stack.size() > 1) throw new RuntimeException("\nInvalid expression: " + pol);

      return stack.pop();
   }

   public static boolean isNumeric(String str) {
      try {
         Double.parseDouble(str);
         return true;
      } catch(NumberFormatException e){
         return false;
      }
   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here

      System.out.println();
      rpn = "2 1 - 4 * 6 3 / +";
      System.out.println("RPN: " + rpn);
      res = Tnode.buildFromRPN (rpn);
      System.out.println("TREE: " + res);

      System.out.println();
      rpn = "2 5 SWAP -";
      System.out.println("RPN: " + rpn);
      res = Tnode.buildFromRPN(rpn);
      System.out.println("TREE: " + res);

      System.out.println();
      rpn = "2 5 9 ROT - +";
      System.out.println("RPN: " + rpn);
      res = Tnode.buildFromRPN(rpn);
      System.out.println("TREE: " + res);

      System.out.println();
      rpn = "2 5 9 ROT + SWAP -";
      System.out.println("RPN: " + rpn);
      res = Tnode.buildFromRPN(rpn);
      System.out.println("TREE: " + res);
   }
}

